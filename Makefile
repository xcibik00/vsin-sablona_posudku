TEX = pdflatex -interaction=nonstopmode -file-line-error

VARDIR = var_tex

REQUIREMENTS = $(VARDIR)/SLOVAK_var.tex
REQUIREMENTS += $(VARDIR)/CZECH_var.tex
REQUIREMENTS += $(VARDIR)/format.tex

.PHONY: all clean

all : Main.pdf

Main.pdf : $(REQUIREMENTS)
	$(TEX) Main.tex
	$(RM) Main.pdf
	$(TEX) Main.tex

clean:
	$(RM) -r *.aux *.bbl *.blg *.log *.nav *.lof *.lol* *.lot *.out *.snm *.toc *.toa *.synctex.gz
	$(RM) *.pdf

